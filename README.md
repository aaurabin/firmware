# drm/firmware

This repository contains the firmware required by the Linux kernel for DRM
firmware.  It is a fork of the [linux-firmware](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/)
repository that is used specifically for staging updated DRM firmware.

## Branches
The `overview` branch is only used for this `README.md` file.

The `main` branch tracks the upstream [linux-firmware](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/) repository. This branch is not touched by
this repository and is for reference only.

All vendors commit updated firmware to their approriate `-staging` branch.
These branches are used for pull requests into the upstream [linux-firmware](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/) repository.

```mermaid
gitGraph
   commit
   commit
   branch amd-staging
   checkout amd-staging
   commit
   commit
   checkout main
   branch intel-staging
   checkout intel-staging
   commit
   commit
   checkout main
   merge amd-staging
   commit
   commit
   checkout main
   merge intel-staging
   branch overview
```

## Submitting upstream pull requests

In order to submit a pull request, do the following steps:

* Commit your firmware changes to `vendor-staging`
* Create a tag in the format `vendor-YYYY-MM-DD` on `vendor-staging`
* Push your changes so that its available on Gitlab
* Send a git pull request via email to `linux-firmware@kernel.org`

Following vendors are available:

* `amd`
* `intel`
